const messageData = {
    position: null,
    service: null
}

function mapLink(pos) {
    return  `https://osm.org/go/${makeShortCode(pos.coords.latitude, pos.coords.longitude, 19)}`;
}

function hrefSMS() {
    return encodeURI(`sms:512989968?body=Potrzebna jest pomoc ${dictionary[messageData.service]}. ${mapLink(messageData.position)}`).replace(/\//g,'%2F')
}

function hrefUpdate() {
    let messageReady = Object.values(messageData).every(el => el)
    if (messageReady) {
        document.querySelector('#send-sos').setAttribute('href', hrefSMS())
    }
}

window.onload = () => {
    navigator.geolocation.watchPosition((pos) => {
        let acc = pos.coords.accuracy;
        document.querySelector('#location-status').style.background = `rgb(${acc}, ${255-acc},0)`
        messageData.position = pos
        document.querySelector('#accuracy').innerText = `${Math.floor(acc)}m`
        document.querySelector('#position').innerText = `${pos.coords.latitude} ${pos.coords.longitude}`
        hrefUpdate()
    })
    
    for (var element of document.querySelectorAll('.service')) {
        element.addEventListener('click', (ev) => {
            messageData.service = ev.target.id
            
            document.querySelectorAll('.service').forEach((el)=>el.classList.remove('checked'))
            ev.target.classList.add('checked')
            hrefUpdate()
        })
    }
}

function makeShortCode(lat, lon, zoom) {
    char_array = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_~";
    var x = Math.round((lon + 180.0) * ((1 << 30) / 90.0));
    var y = Math.round((lat +  90.0) * ((1 << 30) / 45.0));
    // JavaScript only has to keep 32 bits of bitwise operators, so this has to be
    // done in two parts. each of the parts c1/c2 has 30 bits of the total in it
    // and drops the last 4 bits of the full 64 bit Morton code.
    var str = "";
    var c1 = interlace(x >>> 17, y >>> 17), c2 = interlace((x >>> 2) & 0x7fff, (y >>> 2) & 0x7fff);
    for (var i = 0; i < Math.ceil((zoom + 8) / 3.0) && i < 5; ++i) {
        digit = (c1 >> (24 - 6 * i)) & 0x3f;
        str += char_array.charAt(digit);
    }
    for (var i = 5; i < Math.ceil((zoom + 8) / 3.0); ++i) {
        digit = (c2 >> (24 - 6 * (i - 5))) & 0x3f;
        str += char_array.charAt(digit);
    }
    for (var i = 0; i < ((zoom + 8) % 3); ++i) {
        str += "-";
    }
    return str;
}

function interlace(x, y) {
    x = (x | (x << 8)) & 0x00ff00ff;
    x = (x | (x << 4)) & 0x0f0f0f0f;
    x = (x | (x << 2)) & 0x33333333;
    x = (x | (x << 1)) & 0x55555555;

    y = (y | (y << 8)) & 0x00ff00ff;
    y = (y | (y << 4)) & 0x0f0f0f0f;
    y = (y | (y << 2)) & 0x33333333;
    y = (y | (y << 1)) & 0x55555555;

    return (x << 1) | y;
}

const dictionary = {
    'police': 'policji',
    'ambulance': 'medyczna',
    'firefighters': 'straży pożarnej'
}